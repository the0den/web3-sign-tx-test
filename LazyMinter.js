// Source: https://github.com/ipfs-shipyard/nft-school-examples/blob/main/lazy-minting/lib/LazyMinter.js
const ethers = require('ethers')

// These constants must match the ones used in the smart contract.
const SIGNING_DOMAIN_NAME = 'Virtulos Auth NFT service'
const SIGNING_DOMAIN_VERSION = '1'
const PROMISE_TYPE = [
    { name: 'token_id', type: 'uint256' },
    { name: 'uri', type: 'string' },
    { name: 'minimum_price', type: 'uint256' },
    { name: 'creator_eth_address', type: 'address' },
    { name: 'currency', type: 'address' },
]

/**
 * JSDoc typedefs.
 *
 * @typedef {object} NFTVoucher
 * @property {ethers.BigNumber | number} tokenId the id of the un-minted NFT
 * @property {ethers.BigNumber | number} minPrice the minimum price (in wei) that the creator will accept to redeem this NFT
 * @property {string} uri the metadata URI to associate with this NFT
 * @property {ethers.BytesLike} signature an EIP-712 signature of all fields in the NFTVoucher, apart from signature itself.
 */

/**
 * LazyMinter is a helper class that creates NFTVoucher objects and signs them, to be redeemed later by the LazyNFT contract.
 */
class LazyMinter {

    /**
     * Create a new LazyMinter targeting a deployed instance of the LazyNFT contract.
     *
     * @param {Object} options
     * @param {ethers.Contract} contract an ethers Contract that's wired up to the deployed contract
     * @param {ethers.Signer} signer a Signer whose account is authorized to mint NFTs on the deployed contract
     */
    constructor({ contract, signer }) {
        this.contract = contract
        console.log('constructor', this.contract, contract)
        this.signer = signer
    }

    /**
     * Creates a new NFTVoucher object and signs it using this LazyMinter's signing key.
     *
     * @param {ethers.BigNumber | number} tokenId the id of the un-minted NFT
     * @param {string} uri the metadata URI to associate with this NFT
     * @param {ethers.BigNumber | number} minPrice the minimum price (in wei) that the creator will accept to redeem this NFT. defaults to zero
     *
     * @returns {NFTVoucher}
     */
    async createVoucher(tokenId, uri, minPrice = 0) {
        const promise = {
            /*
             - Anything you want. Just a JSON Blob that encodes the data you want to send
             - No required fields
             - This is DApp Specific
             - Be as explicit as possible when building out the message schema.
            */
            token_id: tokenId,
            uri: uri,
            minimum_price: minPrice,
            creator_eth_address: '0x756AA4275FEe8E747beE2d33EC24aab6a1fbba45',
            currency: '0x0000000000000000000000000000000000000000',
        };
        const domain = await this._signingDomain()
        const types = {
            NFTPromise: PROMISE_TYPE,
        }
        console.log('Signing typed data:', domain, types, promise)
        const signature = await this.signer._signTypedData(domain, types, promise)
        return {
            ...promise,
            signature,
        }
    }

    /**
     * @private
     * @returns {object} the EIP-721 signing domain, tied to the chainId of the signer
     */
    async _signingDomain() {
        // console.log('signing domain', this.contract)
        if (this._domain != null) {
            return this._domain
        }
        const chainId = (await this.contract.provider.getNetwork()).chainId
        this._domain = {
            chainId: 1,//chainId,
            name: SIGNING_DOMAIN_NAME,
            verifyingContract: this.contract.address,
            version: SIGNING_DOMAIN_VERSION,
        }
        return this._domain
    }
}

module.exports = {
    LazyMinter,
    SIGNING_DOMAIN_NAME,
    PROMISE_TYPE,
}
