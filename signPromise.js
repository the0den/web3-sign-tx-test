// This one is based on ehterjs
// npm install --save ethers
// Useful SO answer: https://ethereum.stackexchange.com/a/123516

// JSON ABI (copied from Python)
const CONTRACT_ABI = [
    {
        "inputs": [],
        "stateMutability": "nonpayable",
        "type": "constructor"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "account",
                "type": "address"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "operator",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "bool",
                "name": "approved",
                "type": "bool"
            }
        ],
        "name": "ApprovalForAll",
        "type": "event"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "tokenId",
                "type": "uint256"
            }
        ],
        "name": "buyToken",
        "outputs": [],
        "stateMutability": "payable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "tokenURI",
                "type": "string"
            },
            {
                "internalType": "uint256",
                "name": "price",
                "type": "uint256"
            },
            {
                "internalType": "address",
                "name": "seller",
                "type": "address"
            }
        ],
        "name": "mintToken",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "newTokenId",
                "type": "uint256"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "from",
                "type": "address"
            },
            {
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "internalType": "uint256[]",
                "name": "ids",
                "type": "uint256[]"
            },
            {
                "internalType": "uint256[]",
                "name": "amounts",
                "type": "uint256[]"
            },
            {
                "internalType": "bytes",
                "name": "data",
                "type": "bytes"
            }
        ],
        "name": "safeBatchTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "from",
                "type": "address"
            },
            {
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            },
            {
                "internalType": "uint256",
                "name": "amount",
                "type": "uint256"
            },
            {
                "internalType": "bytes",
                "name": "data",
                "type": "bytes"
            }
        ],
        "name": "safeTransferFrom",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "operator",
                "type": "address"
            },
            {
                "internalType": "bool",
                "name": "approved",
                "type": "bool"
            }
        ],
        "name": "setApprovalForAll",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "operator",
                "type": "address"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "from",
                "type": "address"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "uint256[]",
                "name": "ids",
                "type": "uint256[]"
            },
            {
                "indexed": false,
                "internalType": "uint256[]",
                "name": "values",
                "type": "uint256[]"
            }
        ],
        "name": "TransferBatch",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "address",
                "name": "operator",
                "type": "address"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "from",
                "type": "address"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            },
            {
                "indexed": false,
                "internalType": "uint256",
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "TransferSingle",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": false,
                "internalType": "string",
                "name": "value",
                "type": "string"
            },
            {
                "indexed": true,
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            }
        ],
        "name": "URI",
        "type": "event"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "account",
                "type": "address"
            },
            {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            }
        ],
        "name": "balanceOf",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "",
                "type": "uint256"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address[]",
                "name": "accounts",
                "type": "address[]"
            },
            {
                "internalType": "uint256[]",
                "name": "ids",
                "type": "uint256[]"
            }
        ],
        "name": "balanceOfBatch",
        "outputs": [
            {
                "internalType": "uint256[]",
                "name": "",
                "type": "uint256[]"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            }
        ],
        "name": "exists",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "account",
                "type": "address"
            },
            {
                "internalType": "address",
                "name": "operator",
                "type": "address"
            }
        ],
        "name": "isApprovedForAll",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "tokenId",
                "type": "uint256"
            }
        ],
        "name": "isSold",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "name": "sellingTokens",
        "outputs": [
            {
                "components": [
                    {
                        "internalType": "address",
                        "name": "seller",
                        "type": "address"
                    },
                    {
                        "internalType": "uint256",
                        "name": "price",
                        "type": "uint256"
                    },
                    {
                        "internalType": "bool",
                        "name": "sold",
                        "type": "bool"
                    }
                ],
                "internalType": "struct VAuthTokens.MarketInfo[]",
                "name": "items",
                "type": "tuple[]"
            },
            {
                "internalType": "uint256[]",
                "name": "tids",
                "type": "uint256[]"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "bytes4",
                "name": "interfaceId",
                "type": "bytes4"
            }
        ],
        "name": "supportsInterface",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "id",
                "type": "uint256"
            }
        ],
        "name": "totalSupply",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "",
                "type": "uint256"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "tokenId",
                "type": "uint256"
            }
        ],
        "name": "uri",
        "outputs": [
            {
                "internalType": "string",
                "name": "",
                "type": "string"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    }
]


async function main() {

    require('dotenv').config();
    const { API_URL, PRIVATE_KEY } = process.env;
    // const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
    // const web3 = createAlchemyWeb3(API_URL);
    const { ethers } = require("ethers");
    const provider = new ethers.providers.AlchemyProvider("goerli", API_URL);
    const chainId = 1;//(await provider.getNetwork()).chainId
    const wallet = new ethers.Wallet(PRIVATE_KEY, provider);

    const CONTRACT_ADDRESS = '0xD4Fc541236927E2EAf8F27606bD7309C1Fc2cbee'
    const myDeployedContract = new ethers.Contract(
        CONTRACT_ADDRESS,
        CONTRACT_ABI,
        provider,
    )
    // console.log(myDeployedContract, myDeployedContract.getChainID, chainId)

    // Defining the message signing data content.
    promise = {
        /*
         - Anything you want. Just a JSON Blob that encodes the data you want to send
         - No required fields
         - This is DApp Specific
         - Be as explicit as possible when building out the message schema.
        */
        token_id: 14,
        uri: 'http://localhost:8000/media/metadata/kbl_ptzNNOK.json',
        minimum_price: 250,
        creator_eth_address: '0x756AA4275FEe8E747beE2d33EC24aab6a1fbba45',
        currency: '0x0000000000000000000000000000000000000000',
    };

    const {
        LazyMinter, SIGNING_DOMAIN_NAME, PROMISE_TYPE,
    } = require('./LazyMinter.js')
    const lazyminter = new LazyMinter({
        contract: myDeployedContract, signer: wallet
    })

    // This is basically _signTypedData approach.
    // https://docs.ethers.io/v5/single-page/#/v5/api/signer/-%23-Signer-signTypedData
    // It signs hash of the typed data.
    const result = await lazyminter.createVoucher(
        promise.token_id,
        promise.uri,
        promise.minimum_price,
    )
    console.log('result_signed', result)
    /*
    [14,"http://localhost:8000/media/metadata/kbl_ptzNNOK.json",250,"0x756AA4275FEe8E747beE2d33EC24aab6a1fbba45","0x0000000000000000000000000000000000000000","0xf4aeae365e5b52d8f34785a5735a9504d20e5c9643d6ec467c0dfff37f3931103136e84be668858cad9a73b8ebcfeebf1dae390e0b85e889711f56320704e4191c"]
    result_signed {
      token_id: 14,
      uri: 'http://localhost:8000/media/metadata/kbl_ptzNNOK.json',
      minimum_price: 250,
      creator_eth_address: '0x756AA4275FEe8E747beE2d33EC24aab6a1fbba45',
      currency: '0x0000000000000000000000000000000000000000',
      signature: '0xf4aeae365e5b52d8f34785a5735a9504d20e5c9643d6ec467c0dfff37f3931103136e84be668858cad9a73b8ebcfeebf1dae390e0b85e889711f56320704e4191c'
    }
    */

    // TypedDataEncoder approach
    // https://docs.ethers.io/v5/single-page/#/v5/api/utils/hashing/-%23-TypedDataEncoder
    console.log('!!! imported const:', SIGNING_DOMAIN_NAME, LazyMinter)
    const domain = {
        // Defining the chain aka Rinkeby testnet or Ethereum Main Net
        chainId: chainId,
        // Give a user friendly name to the specific contract you are signing for.
        name: SIGNING_DOMAIN_NAME,
        // If name isn't enough add verifying contract to make sure you are establishing contracts with the proper entity
        verifyingContract: CONTRACT_ADDRESS,
        // Just let's you know the latest version. Definitely make sure the field name is correct.
        version: '1',
    };

    const types = {
        // Refer to PrimaryType
        //
        // - uint256 token_id*
        // - address currency; // using the zero address means Ether
        // - uint256 minimum_price*;
        // - address creator: the address to receive the price.
        NFTPromise: PROMISE_TYPE,
    };
    console.log('Encoding typed data:', domain, types, promise)
    // https://github.com/ethers-io/ethers.js/blob/master/packages/hash/src.ts/typed-data.ts#L396
    console.log('hashStruct:', ethers.utils._TypedDataEncoder.from(types).hash(promise))
    // https://github.com/ethers-io/ethers.js/blob/master/packages/hash/src.ts/typed-data.ts#L395
    console.log('domainSeparator:', ethers.utils._TypedDataEncoder.hashDomain(domain))
    // hash = keccak256(TDE.encode)
    const typedResult = ethers.utils._TypedDataEncoder.hash(domain, types, promise)
    console.log('result_typed', typedResult)
}


main();
